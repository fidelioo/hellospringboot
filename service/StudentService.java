package HelloSpringBoot.demomc9.service;

import HelloSpringBoot.demomc9.dto.StudentDto;

import java.util.List;

public interface StudentService {
    List<StudentDto> getStudents();

    StudentDto createStudent(StudentDto dto);

    void deleteStudent(Long id);

    StudentDto updateStudent(StudentDto dto);
}
