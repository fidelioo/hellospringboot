package HelloSpringBoot.demomc9.service;

import HelloSpringBoot.demomc9.dto.StudentDto;
import HelloSpringBoot.demomc9.model.Student;
import HelloSpringBoot.demomc9.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository repository;
    private final ModelMapper mapper;

    @Override
    public List<StudentDto> getStudents() {
        List<Student> all = repository.findAll();
        List<StudentDto> dto = all.stream()
                .map(user -> mapper.map(user, StudentDto.class))
                .collect(Collectors.toList());
        return dto;
    }


    @Override
    public StudentDto createStudent(StudentDto dto) {
        Student student = mapper.map(dto, Student.class);
        Student save = repository.save(student);
        return mapper.map(save, StudentDto.class);
    }

    @Override
    public void deleteStudent(Long id) {
        repository.deleteById(id);

    }

    @Override
    public StudentDto updateStudent(StudentDto dto) {
        Student student = repository.findById(dto.getId()).orElseThrow((() -> new RuntimeException("Student not found")));
        student.setName(dto.getName());
        student.setInstitute(dto.getInstitute());
        student.setBirthdate(dto.getBirthdate());
        Student save = repository.save(student);
        return mapper.map(save, StudentDto.class);
    }
}
