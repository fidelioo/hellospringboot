package HelloSpringBoot.demomc9.Controller;

import HelloSpringBoot.demomc9.dto.StudentDto;
import HelloSpringBoot.demomc9.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService service;


    @GetMapping("/all")
    public List<StudentDto> getStudents() {

        return service.getStudents();
    }

    @PostMapping
    public StudentDto createStudent(@RequestBody StudentDto dto) {
        return service.createStudent(dto);
    }

    @PutMapping
    public StudentDto updateStudent(@RequestBody StudentDto dto) {
        return service.updateStudent(dto);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id) {
        service.deleteStudent(id);
    }

}
