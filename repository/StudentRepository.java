package HelloSpringBoot.demomc9.repository;

import HelloSpringBoot.demomc9.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Long> {

}
